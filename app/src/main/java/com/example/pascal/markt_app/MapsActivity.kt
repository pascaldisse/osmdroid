package com.example.pascal.markt_app

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_maps.*
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.cachemanager.CacheManager
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.BoundingBox
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus
import org.osmdroid.views.overlay.OverlayItem


class MapsActivity : AppCompatActivity(), CacheManager.CacheManagerCallback, View.OnClickListener {

    val TAG = "MapsActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Configuration.getInstance().load(applicationContext, PreferenceManager.getDefaultSharedPreferences(applicationContext))
        //important! set your user agent to prevent getting banned from the osm servers
        Configuration.getInstance().setUserAgentValue(packageName)
        Log.d(TAG, packageName)


        //important: has to be after configuration
        setContentView(R.layout.activity_maps)

        download_map_button.setOnClickListener(this)

        setupMapSettings(map_view)
        setupMapMarkers(map_view)
    }

    public override fun onResume() {
        super.onResume()
        //this will refresh the osmdroid configuration on resuming.
        Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this))
    }

    fun setupMapSettings(map: MapView) {

        map.setTileSource(TileSourceFactory.MAPNIK)

        //add zoom buttons and zoom with 2 fingers:
        map.setBuiltInZoomControls(true)
        map.setMultiTouchControls(true)

        //setup camera position and zoom
        val mapController = map.controller
        mapController.setZoom(18)
        val startPoint = GeoPoint(52.5145, 13.3501)
        mapController.setCenter(startPoint)
    }

    fun setupMapMarkers(map: MapView) {
        val items = ArrayList<OverlayItem>()
        items.add(OverlayItem("Title", "Description", GeoPoint(52.5145, 13.3501)))

        val mapOverlay = ItemizedOverlayWithFocus<OverlayItem>(this, items, MarketMarkerOverlay())

//        mapOverlay.setFocusItemsOnTap(true) //adds a pupup to the marker
        map.getOverlays().add(mapOverlay)
    }

    override fun onClick(p0: View?) {

        when (p0?.id) {
            download_map_button.id -> {
                downloadMap()
            }
        }


    }

    fun downloadMap() {

        // Here, is the permission granted?
        if (!hasPermissionWriteExternalStorage()){
            requestPermissionExternalStorage(MAP_DOWNLOAD_WRITE_EXTERNAL_STORAGE)
        } else {
            val cacheManager = CacheManager(map_view)
            val bounds = BoundingBox(52.54045, 13.440988, 52.494906, 13.322112);
//            TODO: sometimes stops, check osmdroid for cachemanager example
            cacheManager.downloadAreaAsync(this, bounds, 18, 18, this);
        }

    }



    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            MAP_DOWNLOAD_WRITE_EXTERNAL_STORAGE -> {
                if (permissionGranted(grantResults)) {
                    downloadMap()
                } else {
                    // TODO: permission denied. Maybe show an error message to the user here
                }
                return
            }
        }
    }

    override fun setPossibleTilesInArea(total: Int) {
        Log.d("CacheManager", "setPossibleTilesInArea $total")
    }

    override fun onTaskComplete() {
        Log.d("CacheManager", "Download successful")
    }

    override fun onTaskFailed(errors: Int) {
        Log.d("CacheManager", "Download failed with Error: $errors")
    }

    override fun updateProgress(progress: Int, currentZoomLevel: Int, zoomMin: Int, zoomMax: Int) {
        Log.d("CacheManager", "Download progress: $progress Zoom level $currentZoomLevel")
    }

    override fun downloadStarted() {
        Log.d("CacheManager", "Download started")
    }

    fun hasPermissionWriteExternalStorage(): Boolean{
        val permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return (permissionCheck == PackageManager.PERMISSION_GRANTED)
    }

    fun requestPermissionExternalStorage(requestCode: Int){
        ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                requestCode)
    }

    fun permissionGranted(grantResults: IntArray): Boolean{
        // If request is cancelled, the result arrays are empty.
        return grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
    }
}
