package com.example.pascal.markt_app

import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import org.osmdroid.views.overlay.ItemizedIconOverlay
import org.osmdroid.views.overlay.OverlayItem


/**
 * Created by Pascal on 2018-01-01.
 */
class MarketMarkerOverlay: ItemizedIconOverlay.OnItemGestureListener<OverlayItem> {
    val TAG = "MarketMarkerOverlay"


    override fun onItemSingleTapUp(index: Int, item: OverlayItem?): Boolean {
        //TODO: do add click action
        Log.d(TAG, "Marker Clicked")
        return true
    }
    override fun onItemLongPress(index: Int, item: OverlayItem?): Boolean {
        return false
    }

}